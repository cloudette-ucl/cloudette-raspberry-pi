package com.cloudette.connectivity;


import java.net.ServerSocket;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Arrays;

import com.cloudette.skydrive.AccessToken;
import com.cloudette.skydrive.SkyDrive;

public class ConnectionListener {
	
	
	static Runtime runtime = Runtime.getRuntime();
	
	//Listen for an incoming connection from the user's PC.
	public void listenForIncomingConnection() throws IOException
	{
		String inputFromClient;
		String responseToClient = null;
		ServerSocket welcomeSocket = new ServerSocket(6789);
		TextFileReader rdr = new TextFileReader();
		
		while(true)
		{
			Socket connectionSocket = welcomeSocket.accept();
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
			inputFromClient = inFromClient.readLine();
			if (inputFromClient.equals("SCAN"))
			{
				System.out.println("Received command to get SSIDs.");
				//run a script to get all information on access points visible to wlan1
				runtime.exec("./getSSIDs");
				//responds to the client
				responseToClient = rdr.readFile("./ssid.txt") + '\n';
				outToClient.writeBytes(responseToClient);
				inFromClient = null;
			}
			else if (inputFromClient.equals("CHOSENSSID"))
			{
				//receives a new interfaces file to be used.
				
					try {
						receiveNewInterfacesFile();
					} catch (ClassNotFoundException e) {
						System.out.println("FUCK!");
					}
				
				//run a script to move the interfaces file and reset wlan1 to use the new settings
				inFromClient = null;
			}
			else
			{
				SkyDrive.accessToken = inputFromClient;
				break;
			}
		}
		welcomeSocket.close();
		
	}
	
	//receive a newly generated interfaces file and then execute script
	private static void receiveNewInterfacesFile() throws IOException, ClassNotFoundException
	{
		BufferedReader inFromClient;
		ServerSocket servsock = new ServerSocket(9876);
		Socket s = servsock.accept();
		System.out.println("Receiving file");
		InputStream stream = s.getInputStream();
		byte[] data = new byte[1000];
		int count = stream.read(data);
		int i = data.length - 1;
		while(i >= 0 && data[i] == 0)
		{
			--i;
		}
		data = Arrays.copyOf(data, i+1);
		FileOutputStream fos = new FileOutputStream(new File("./interfaces"));
		fos.write(data);
		fos.close();
		
		
		//write br to file the end
		
		System.out.println("File received");
		Runtime hello = Runtime.getRuntime();
		hello.exec("./UseNewInterface");
		System.out.println("Script executed.");
		servsock.close();
	}
}
