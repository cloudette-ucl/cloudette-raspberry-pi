package com.cloudette.connectivity;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class TextFileReader {

	public String readFile(String file) throws IOException
	{
		BufferedReader reader = new BufferedReader (new FileReader (file));
		String line = null;
		StringBuilder sb = new StringBuilder();
		String ls = System.getProperty("line.seperator");
		
		while((line = reader.readLine()) != null)
		{
			sb.append(line);
			sb.append("NEWLINE");
		}
		return sb.toString();
		
	}
	
	

}
