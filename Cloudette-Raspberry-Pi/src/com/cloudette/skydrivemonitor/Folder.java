package com.cloudette.skydrivemonitor;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

import javax.ws.rs.core.MediaType;

import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cloudette.filemonitor.FileMonitor;
import com.cloudette.skydrive.SkyDrive;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class Folder {
	
	private String folderName;
	private String id;
	private String directoryMonitored;
	private static String accessToken = SkyDrive.accessToken;
	private ArrayList<Folder> folders = new ArrayList<Folder>();
	
	private Client client = new Client();
	
	public Folder(String folderName, String id, String directoryMonitored)
	{
		this.folderName = folderName;
		this.id = id;
		this.directoryMonitored = directoryMonitored;
		
		try {
			getFolderData();
	
			if (folders.size() > 0)
			{
				for (int i = 0; i < folders.size(); i ++)
				{
					System.out.println(folders.get(i).getFolderName());
					checkList(folders.get(i));
				}
				
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	
	private void checkList(Folder folder) throws NullPointerException
	{
		File directory = new File(this.directoryMonitored + "/" + folder.getFolderName());
		if (directory.exists() == false)
		{
			directory.getParentFile().mkdirs();
			directory.mkdir();
		}
	}
	
	public String getID()
	{
		return id;
	}
	
	public String getFolderName()
	{
		return folderName;
	}
	
	private void getFolderData() throws JSONException
	{
		WebResource webResource = client.resource("https://apis.live.net/v5.0/"+this.id+"/files?access_token="+Folder.accessToken);
		ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
		String output = response.getEntity(String.class);
		JSONObject jsonObj = new JSONObject(output);
		JSONArray data = jsonObj.getJSONArray("data");
		for(int i = 0; i < data.length(); i ++)
		{
			if (data.length() == 0)
			{
				return;
			}
			JSONObject node = data.getJSONObject(i);
			if (node.getString("type").equals("folder"))
			{
				Folder folder = new Folder(node.getString("name"), node.getString("id"),this.directoryMonitored+"/"+node.getString("name"));
				folders.add(folder);
			}
		}
	}


}
