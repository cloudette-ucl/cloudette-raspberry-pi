package com.cloudette.filereceiver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class FileReceiver extends Thread {
	
	public void run()
	{
		while (true) {
		try {

				ServerSocket servsock = new ServerSocket(9878);
				Socket s = servsock.accept();

				ObjectOutputStream oos = new ObjectOutputStream(
						s.getOutputStream());
				ObjectInputStream ois = new ObjectInputStream(
						s.getInputStream());
				
				byte[] buffer = new byte[100];

				Object o = ois.readObject();
				FileOutputStream fos = new FileOutputStream("/media/CLOUDETTE/" + o.toString());

				Integer bytesRead = 0;

				do {
					o = ois.readObject();
					bytesRead = (int) o;
					o = ois.readObject();
					buffer = (byte[]) o;
					fos.write(buffer, 0, bytesRead);
				} while (bytesRead == 100);

		
				fos.close();
				ois.close();
				oos.close();
				servsock.close();
				
		
			} catch (IOException e) {

			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

}
