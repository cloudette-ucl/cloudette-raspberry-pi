package com.cloudette.skydrive;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class SkyDrive {
	
	private static Client client; 	
	public static String accessToken = "EwA4Aq1DBAAUGCCXc8wU/zFu9QnLdZXy%2bYnElFkAAX53/Zbl6JGdx9GJIY4RIrjGYrdXXItMXR5NFlrEQixRs0nmfhMQBBefnrpkQlYHQdXgq5NwwlHRQ16cetoNPIOY0Hkc6G2T11bxmR4MsC2/nRXwiIso23bvS3cAMnXCD7d1660SueUYhHUp4iXMS1DwxU8%2brO1mvrWQRwB76DpjoaybJNVGA14lAFBM5ngoeOK4PEVkRarW94OIKt3GDappkCct16V6X3adRMrH/smE0hW4EPsSVVMC18Wsj6dURP08SxchueakIZSF7STk/TsdQWMWHa7LQj%2bLR6EVeewe3o29IHVVkh%2b8ArEDr5A%2bYYEpONsTZWGOO5GUHgiaSq4DZgAACLbV%2b6nLHG4iCAFGPe8mYrId0G4UpbBMwehSbc1/p0WSXk0GfF6d2KV7n/UOlfnm9xaz9cs5hbxH7w9FDuX4Vln/kNe9%2b6iNIe3WKj15NpGOje0mZ%2bnmSZs5uB6GYpZvE3eVQE4vwNw4FWIB08y1vHAyTGK2FPGKq/s5dglm1i3n3Q2dVsajfslh82fHxgmnRC7FodrRsuG1knPKvd0HeI9SC60izMDnaHguvFs8lUqafYj9BINBklVqtH%2bVP/JdQ%2bgT%2bK/idOSp1xbYPvb2V7AOKFzTdg4Hrgy1V557J56vj41qvCviLtTBzIifBHkTNKILFMjXSimSr%2bDtB3ZraoBgESKDQAN%2bk%2bV3TmlYkIhlcsgAAA%3d%3d";
	
	public static void getFiles()
	{	
		WebResource webResource = client.resource("https://apis.live.net/v5.0/folder.02907595e725076b.2907595E725076B!106/files?access_token="+ accessToken);
		ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
		String output = response.getEntity(String.class);
        System.out.println(output);
        parseString(output);
		//get the file list
		//parse it	
	}
	
	private static void parseString(String input)
	{
		
	}
	
	private static String getFileName(String input)
	{
		int stop = 0;
		for (int i = input.length() - 1; i >= 0; i--)
		{
			if (input.charAt(i) == '/')
			{
				stop = i;
				System.out.println(stop);
				break;
			}
		}
		String fileName = input.substring(stop+1, input.length());
		System.out.println(fileName);
		return fileName;
	}
	
	public static void upload(String fileLocation)
	{
		String fileName = getFileName(fileLocation);
		try {
			uploadFile(fileLocation, fileName);
		} catch (IOException e) {
			e.printStackTrace();
		}

		
	}
	
	private static void createFolder(String fileName)
	{
		//
	}
	
	private static void uploadFile(String fileLocation, String fileName) throws IOException
	{
		final String BOUNDARY_STRING = "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f";
		 
        URL connectURL = new URL(
                "https://apis.live.net/v5.0/me/skydrive/files?"
                        + "access_token=" + accessToken);
        HttpURLConnection conn = (HttpURLConnection) connectURL
                .openConnection();
 
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type",
                "multipart/form-data; boundary=" + BOUNDARY_STRING);
        // set read and write
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
 
        conn.setRequestProperty("Connection", "Keep-Alive");
 
        conn.connect();
 
        // set body of stream
 
        DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
        dos.writeBytes("--" + BOUNDARY_STRING + "\r\n");
        dos.writeBytes("Content-Disposition: form-data; name=\"file\"; filename=\""
                + URLEncoder.encode(fileName, "UTF-8") + "\"\r\n");
        dos.writeBytes("Content-Type: application/octet-stream\r\n");
        dos.writeBytes("\r\n");
 
        FileInputStream fileInputStream = new FileInputStream(new File(fileLocation));
        int fileSize = fileInputStream.available();
        int maxBufferSize = 8 * 1024;
        int bufferSize = Math.min(fileSize, maxBufferSize);
        byte[] buffer = new byte[bufferSize];
 
        // sending file
        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        while (bytesRead > 0) {
 
            // Upload file part(s)
            dos.write(buffer, 0, bytesRead);
 
            int bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, buffer.length);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
        fileInputStream.close();
        //end of file sending
 
        dos.writeBytes("\r\n");
        dos.writeBytes("--" + BOUNDARY_STRING + "--\r\n");
 
        dos.flush();
 
        //obtain JSON
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                conn.getInputStream()));
        String line = null;
        StringBuilder sbuilder = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            sbuilder.append(line);
            System.out.println(line);
        }
	}
	
	

}
