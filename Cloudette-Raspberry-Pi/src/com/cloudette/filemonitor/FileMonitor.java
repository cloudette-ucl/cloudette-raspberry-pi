package com.cloudette.filemonitor;

import java.io.File;

import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

public class FileMonitor extends Thread {

	private String directoryToListen = "/media/CLOUDETTE/";
	public void run() {
		
		final File directory = new File(directoryToListen);
		FileAlterationObserver fao = new FileAlterationObserver(directory);
		fao.addListener(new FileAlterationListenerImpl());

		//check database to see if you need to initialise more threads.
		
		final FileAlterationMonitor monitor = new FileAlterationMonitor();
		monitor.addObserver(fao);
		try {
			monitor.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
