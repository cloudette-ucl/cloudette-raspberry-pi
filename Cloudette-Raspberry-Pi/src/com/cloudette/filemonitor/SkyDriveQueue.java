package com.cloudette.filemonitor;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.cloudette.skydrive.SkyDrive;

public class SkyDriveQueue extends Thread {
	
	private static ConcurrentLinkedQueue<File> queue = new ConcurrentLinkedQueue<File>();
	
	public static void addToQueue(File inputFile)
	{
		queue.add(inputFile);
	}
	
	public void run()
	{
		while(true)
		{
			if (queue.peek() != null)
			{	
				SkyDrive.upload(queue.poll().toString());
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
		}
	}

}
