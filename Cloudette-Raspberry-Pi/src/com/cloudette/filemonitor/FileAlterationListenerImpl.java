package com.cloudette.filemonitor;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;

import com.cloudette.skydrive.SkyDrive;

public class FileAlterationListenerImpl implements FileAlterationListener {

	@Override
	public void onStart(final FileAlterationObserver observer) {
		
	}

	@Override
	public void onDirectoryCreate(final File directory) {
		System.out.println(directory.getAbsolutePath() + " was created.");
		System.out.println("Must create a new directory on SkyDrive and listen to this directory. Add folder to database.");
		
	}

	@Override
	public void onDirectoryChange(final File directory) {
		//System.out.println(directory.getAbsolutePath() + " was modified");
		//System.out.println("Must rename directory on SkyDrive");
		
	}

	@Override
	public void onDirectoryDelete(final File directory) {
		System.out.println(directory.getAbsolutePath() + " was deleted.");
		System.out.println("Must remove folder from database.");
	}

	@Override
	public void onFileCreate(final File file) {
		System.out.println(file.getAbsoluteFile() + " was created.");
		System.out.println("Must add this file to SkyDrive.");
		SkyDriveQueue.addToQueue(file.getAbsoluteFile());
		
	}

	@Override
	public void onFileChange(final File file) {
		System.out.println(file.getAbsoluteFile() + " was modified.");
		System.out.println("Must add this file to SkyDrive.");
	}

	@Override
	public void onFileDelete(final File file) {
		System.out.println(file.getAbsoluteFile() + " was deleted.");
	}

	@Override
	public void onStop(final FileAlterationObserver observer) {
		
	}

}
