package com.cloudette.main;

import java.io.IOException;

import com.cloudette.connectivity.ConnectionListener;
import com.cloudette.filemonitor.FileMonitor;
import com.cloudette.filemonitor.SkyDriveQueue;
import com.cloudette.filereceiver.FileReceiver;

public class Main {

	public static void main(String[] args) {
		Main main = new Main();
		main.start();
	}
	
	private void start()
	{

		ConnectionListener cl = new ConnectionListener();
		try {
			cl.listenForIncomingConnection();
		} catch (IOException e) {
			return;
		}
		
		
		//we now have a connection, now let's listen for file changes
		FileMonitor fm = new FileMonitor();
		fm.start();
		//start queue
		SkyDriveQueue q = new SkyDriveQueue();
		q.start();
		//and start receiving files
		FileReceiver fr = new FileReceiver();
		fr.start();
	}

}
